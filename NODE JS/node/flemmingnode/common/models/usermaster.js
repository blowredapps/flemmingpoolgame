'use strict';

module.exports = function(Usermaster) {

    let  registrationService=require('../../server/server/register');


    Usermaster.registerplayers = function(playerdata, callback) {
        console.log("dataaa",playerdata)
        registrationService.registerplayers(playerdata, function(err, result) {
            callback(null, result);
        });
    };
    
    Usermaster.remoteMethod('registerplayers', {
        accepts: { arg: 'playerdata', type: 'array' },
        returns: { arg: 'playerdata', type: 'array' },
        http: {
            path: '/registerplayers',
            verb: 'post'
        }
    });
};
