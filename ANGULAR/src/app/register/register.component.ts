import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {LoginserviceService} from '../loginservice.service';
import {Router} from '@angular/router';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  PlayerRegForm: FormGroup;
  disableButton:boolean=false;
  HideMsg:any=true;

  constructor(
    private LoginserviceService:LoginserviceService,
    private Router:Router,
    private toastr: ToastrService
  ) { }


  ngOnInit() {
    this.PlayerRegForm=new FormGroup({
      "firstname": new FormControl(null, Validators.required),
      "middlename": new FormControl(null, Validators.required),
      "lastname": new FormControl(null, Validators.required),
      "email": new FormControl(null, Validators.required),
      "phonenumber": new FormControl(null, Validators.required),
      "studentid": new FormControl(null, Validators.required),
      "dateofbirth": new FormControl(null, Validators.required),
      "password": new FormControl(null, Validators.required),
      "address": new FormControl(null, Validators.required),
    })
  }

  addPost(data,form){
    let countFields = 9;
    let incrementCount = 0;
    Object.keys(form.controls).forEach(field=>{
      const control = form.get(field);
      if (control instanceof FormControl && control.hasError('required')) {
          control.markAsTouched({ onlySelf: true });
      
      } else{
        ++incrementCount
      }
    })
    if(incrementCount === countFields){
      data.status   = "A"; 
      this.disableButton = true;
      this.LoginserviceService.register(data).subscribe(message => {
        this.PlayerRegForm.reset();
        setTimeout(() => {
          this.toastr.success('Success!', 'Registration !')
        });
        this.HideMsg = false;
      })
      this.disableButton = false;
      this.Router.navigate([''])
    }

}
}
