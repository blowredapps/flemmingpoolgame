import { Injectable } from '@angular/core';
import {LoopBackAuth, SDKToken} from '../loopback/shared/sdk';
import { Observable } from 'rxjs';
import {UsermasterApi} from '../loopback/shared/sdk'

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  private sessiontoken:SDKToken=new SDKToken(); // This is for creating token Object....
  private rememberMe:boolean=true;

  constructor(
    private UsermstApi : UsermasterApi
  ) { }

  register(data):Observable<any>{
    return this.UsermstApi.registerplayers(data);
  }

  login(userObj): Observable<any>{
    return this.UsermstApi.login(userObj);
  }

  logout(logoutdata): Observable<any>{
    localStorage.clear();
    return this.UsermstApi.logout();

  }
}
