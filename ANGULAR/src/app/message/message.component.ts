import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LoginserviceService} from '../loginservice.service'
import { SDKToken ,UsermasterApi, User} from '../../loopback/shared/sdk';
import { ActivatedRoute } from '@angular/router';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  profiledataaaa:{};
  constructor(
    private router: Router,
    private LoginserviceService:LoginserviceService,
    private route:ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    let profiledata=localStorage.getItem('user');
    this.profiledataaaa=JSON.parse(profiledata);

  }


  signOut(){
    let logoutdata=localStorage.getItem('userid');
    this.LoginserviceService.logout(logoutdata).subscribe((token: SDKToken)=>{
      this.toastr.success('Success!', 'Logout !',{
        timeOut: 1000,
      })
      this.router.navigate(['']);
    })

    
  }
 
}
