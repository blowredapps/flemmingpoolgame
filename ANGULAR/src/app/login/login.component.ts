import { Component, OnInit } from '@angular/core';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SDKToken ,UsermasterApi} from '../../loopback/shared/sdk';
import {LoginserviceService} from '../loginservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private usermst: UsermasterApi,
    private LoginserviceService: LoginserviceService,
    private router: Router,
    private toastr: ToastrService
  ) { }
  

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required])
    })
    // localStorage.clear()  
  }

  login(form) {
    let email = this.loginForm.controls['email'].value;
    let password = this.loginForm.controls['password'].value;

    let loginObj = { 'email': email, 'password': password }
   this.LoginserviceService.login(loginObj).subscribe((token: SDKToken) => {


      this.toastr.success('loading ....', 'Login Successfull', {
        timeOut: 3000,
        // positionClass: 'toast-top-center',
        progressBar: true,
        progressAnimation: 'increasing'
      });


     console.log(token)
      localStorage.setItem('user', JSON.stringify(token.user));
      localStorage.setItem('userid', JSON.stringify(token.id));
     
      const local=localStorage.getItem('user');
      console.log(local)

      setTimeout(() => {
        this.router.navigate(['message'])
      }, 3000)


    }, (err) => {
      console.log(err)
      if (err.statusCode === 401) {
        this.toastr.error('Error-401....Invalid Credentials', 'Login Failed', {
          timeOut: 8000,
          positionClass: 'toast-top-center'
        });
      }
    })
  }
  valSignInForm(loginForm: FormGroup) {
    const errorCount = 2;
    let triggerSignIn = 0;
    Object.keys(loginForm.controls).forEach(field => {
      const control = loginForm.get(field);
      if (control instanceof FormControl && control.hasError('required')) {
      } else {
        ++triggerSignIn;
      }
    })
    if (triggerSignIn === errorCount) {
      // this.router.navigate(['dashboard']);
      this.login(loginForm)
    }
  }

}
